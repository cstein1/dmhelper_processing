# README #

This project is intended to make world creation easy and intuitive, particularly for those creating scenes for Dungeons And Dragons. 
We do not own any form of rights to any Wizards of the Coast's products; however, we encourage you to check them out!



### What are the details of this project? ###

V 0.0
The general idea is that you click NEW WORLD and are greeted by a discretized grid. 
Each cell is highlighted as you scroll over it. 
If you left click, then a menu pops up and you can click an object to create a dungeon entrance, house, etc... 
You can also add text to each cell as reminder text for the DM which pops up as you scroll over a cell.
A 'player mode' will also be available. You can display it up on a tv or something separate from your screen and reveal spaces as your team sees them. 
The player mode will remove reminder text and have some extra features.
You will also be able to store a list of characters within cells and the player mode will allow you to move those characters around easily with hotkeys (think starcraft?).

### How do I get set up? ###


### Contribution guidelines ###


### Who do I talk to? ###
Creator and Maintainer: Charles Stein <cstein1 *AT* trinity *DOT* edu>