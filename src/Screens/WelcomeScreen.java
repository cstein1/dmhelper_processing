package Screens;

import processing.core.PApplet;
import Resources.*;
// Welcome Screen which will have options Settings, Load World, and New World

public class WelcomeScreen extends Screen {
	PApplet parent;
	Button[] btns;

	public WelcomeScreen(PApplet p) {
		parent = p;
		float wd = parent.width;
		float ht = parent.height;
		btns = new Button[] {
				new Button(parent, wd / 2, ht / 2, 100, 50, parent.color(200, 200, 200), parent.color(100, 100, 99),
						"New World", "design"),
				new Button(parent, wd / 2, ht / 2 + ht / 8, 100, 50, parent.color(200, 200, 200), parent.color(100, 100, 99),
						"Load World", "load"), // This needs to send us to the LoadWorldScreen, and this screen needs to
												// be able to decrypt worlds
				new Button(parent, wd / 2, ht / 2 + ht / 4, 100, 50, parent.color(200, 200, 200), parent.color(100, 100, 99),
						"Settings", "settings") // This needs to send us to the SettingsScreen
		};
	}

	public void show() {
		parent.background(0);
		parent.textSize(45);
		parent.text("WELCOME TO DMHELPER", parent.width / 2, parent.height / 8);
		for (Button b : btns) {
			b.display();
		}
	}
}