package Screens;

import processing.core.PApplet;
import Resources.*;

// This class visualizes the cells and shows tools (buttons) which will allow us manipulate the style of our editing tools

// For example, if you want to select a single cell, then you are in 1Select mode (needs better name), 
// but if you want to select multiple, then you are in MultiSelect Mode and you can switch between the two via buttons on the side

public class DesignScreen extends Screen {
	PApplet parent;
	public boolean space = false;
	Button[] btns;
	Cell[][] cells;

	float scl; // scale
	float trn; // translated (has to always be width*(1/scale's denominator)

	// visualization properties
	int numCellsX, numCellsY;
	int x, y;
	boolean hov = false;
	boolean hovClick = false; // Are we hovering AND clicking?

	public DesignScreen(PApplet p) {
		parent = p;
		trn =  parent.width / 8;
		scl = (float)7/8;
		numCellsX = 50;
		numCellsY = 50;

		x = numCellsX / 2;
		y = numCellsY / 2;

		// x by y
		cells = new Cell[numCellsX][numCellsY];
		for (int i = 0; i < numCellsX; i++) {
			for (int j = 0; j < numCellsY; j++) {
				int newcolor = parent.color(
						PApplet.map(i, 0, numCellsX, 100, 200), 
						PApplet.map(j,0,numCellsY, 100, 200), 
						200);
				cells[i][j] = new Cell(parent, i, j, newcolor);
			}
		}
	}

	public void show() {
		parent.fill(255);
		parent.stroke(0);
		// Draw the Canvas Area
		parent.pushMatrix();
		
		parent.translate(trn, 0);
		parent.scale(scl);
		drawCanvas();
		drawCells(); // TODO (currently draws whitespace with text and gridlines. Needs to draw
						// cells, but cells currently have no shape
		parent.popMatrix();
		parent.fill(255);
		parent.stroke(255);
		parent.text("Click and drag to move around!", parent.width/2, parent.height*14/15);
		// Draw the Tool Selection Area
		drawTools(); // TODO
	}

	void drawCanvas() {
		// - Draw Canvas Outline
		parent.line(0, 0, 0, parent.height);
		parent.line(0, parent.height, parent.width, parent.height);
	}

	void drawCells() {
		// - Draw The Grid with Cells
		// - - Num of cells to visualize
		int visCellX = 5;
		int visCellY = 5;

		float mx = parent.mouseX;
		float pmx = parent.pmouseX;
		float my = parent.mouseY;
		float pmy = parent.pmouseY;
		float pwd = parent.width;
		float pht = parent.height;

		//if (parent.keyPressed) {
			//if (parent.key == ' ') {
				if (parent.mousePressed && parent.mouseButton ==PApplet.LEFT) {
					if (mx - pmx > 0 && x + visCellX < numCellsX)
						x += 1;
					if (mx - pmx < 0 && x > 0)
						x -= 1;
					if (my - pmy > 0 && y + visCellY < numCellsY)
						y += 1;
					if (my - pmy < 0 && y > 0)
						y -= 1;
				}
			//}
		//}
		for (int i = 0; i < visCellX; i++) {
			for (int j = 0; j < visCellY; j++) {
				float iloc = PApplet.map(i, 0, visCellX, 0, pwd);
				float jloc = PApplet.map(j, 0, visCellY, 0, pht);
				parent.fill(255);
				float nmouseX = (mx - trn) / scl;
				float nmouseY = my / scl;
				float wd = pwd / visCellX;
				float ht = pht / visCellY;
				if (nmouseX > iloc && nmouseX < iloc + wd && nmouseY > jloc && nmouseY < jloc + ht)
					hov = true;
				else
					hov = false;
				if (hov) {
					if (parent.mousePressed && parent.mouseButton == PApplet.RIGHT) {
						hovClick = true;
					} else
						hovClick = false;
					cells[i + x][j + y].display(iloc, jloc, wd, ht, true);
				} else
					cells[i + x][j + y].display(iloc, jloc, wd, ht);
				parent.fill(0);
				parent.text(PApplet.str(i + x) + "," + PApplet.str(j + y), iloc + wd / 2, jloc + ht / 2);

				if (hovClick) {
					parent.fill(100);
					parent.rect(nmouseX, nmouseY, 100, 100);
				}
			}
		}
	}
	

	void drawMenu() {
		// TODO
	}

	void drawTools() {
		// TODO
	} 
}