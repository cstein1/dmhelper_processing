import processing.core.PApplet;
import java.util.*;

import Screens.*;

public class Main extends PApplet {
	public static void main(String[] args) {
		PApplet.main("Main");
	}

	// `disp tells us which screen we are currently visualizing
	HashMap<String, Screen> disp;

	// `vis is the key which tells us where we are in `disp
	String vis;
	boolean testing = true;
	String testScreen = "design";

	public void settings() {
		size(800, 800);
	}

	public void setup() {
		disp = new HashMap<String, Screen>();
		disp.put("design", new DesignScreen(this));
		disp.put("welcome", new WelcomeScreen(this));
		vis = "welcome";
	}

	public void draw() {
		background(0);
		Screen scr;
		if (testing) {
			scr = disp.get(testScreen);
		} else {
			scr = disp.get(vis);
		}

		// Visualize the screen of interest, denoted by `vis
		scr.show();
	}
}
