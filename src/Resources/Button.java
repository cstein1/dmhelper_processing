package Resources;

import processing.core.PApplet;
import processing.core.PVector;

public class Button {
	PApplet parent;
	PVector loc, size;
	int ocol, ncol; // original color vs new color (when we are hovering over the element) 
	String txt, nScr; // Text displayed on the button... `nScr gives us the string key for the next
						// screen

	public Button(PApplet p, PVector _loc, PVector _size, int _ocolor, int _ncolor, String _txt, String newScreen) {
		parent = p;
		loc = _loc;
		size = _size;
		ocol = _ocolor;
		ncol = _ncolor;
		txt = _txt;
		nScr = newScreen;
	}

	public Button(PApplet p, float x, float y, float sx, float sy, int _ocolor, int _ncolor, String _txt,
			String newScreen) {
		parent = p;
		loc = new PVector(PApplet.floor(x), PApplet.floor(y));
		size = new PVector(PApplet.floor(sx), PApplet.floor(sy));
		ocol = _ocolor;
		ncol = _ncolor;
		txt = _txt;
		nScr = newScreen;
	}

	public void display() {
		if (hovering()) {
			parent.fill(ncol);
		} else {
			parent.fill(ocol);
		}

		parent.rectMode(PApplet.CENTER);
		parent.rect(loc.x, loc.y, size.x, size.y);
		parent.fill(0, 0, 255);
		int txtsz = 16;
		parent.textSize(txtsz);
		parent.textAlign(PApplet.CENTER);
		parent.text(txt, loc.x, loc.y + txtsz / 2);

	}

	boolean hovering() {
		float hx = size.x / 2;
		float hy = size.y / 2;
		float mx = parent.mouseX;
		float my = parent.mouseY;
		return (mx > loc.x - hx && mx < loc.x + hx) && (my > loc.y - hy && my < loc.y + hy);
	}
}
