package Resources;

import processing.core.PApplet;

// A class for storing information in each cell within the design screen
public class Cell {
	PApplet parent;
	String info;
	int i, j; // Indices
	int tex; // We want to put the texture in a folder and keep track of it by index so that
				// we don't have to intialize a PImage every time we make a cell
	int col;
	// Array of components

	public Cell(PApplet p, int _i, int _j) {
		parent = p;
		i = _i;
		j = _j;
		tex = 0;
		col = parent.color(250, 231, 181);
	}

	public Cell(PApplet p, int _i, int _j, int _col) {
		parent = p;
		i = _i;
		j = _j;
		tex = 0;
		col = _col;
	}

	public void display(float xpos, float ypos, float wth, float hgt) {
		if (tex == 0) {
			parent.fill(col);
			parent.rect(xpos, ypos, wth, hgt); // We want this to be drawing a cell eventually (when the cells actually
												// get individual personalities)
		} else {
			// TODO: Visualize with texture
		}
	}

	public void display(float xpos, float ypos, float wth, float hgt, boolean hghlt) {
		if (tex == 0) {
			if (hghlt) {
				parent.fill(col, 200);
			}
			parent.rect(xpos, ypos, wth, hgt); // We want this to be drawing a cell eventually (when the cells actually
												// get individual personalities)
		} else {
			// TODO: Visualize with texture
		}
	}

	public void rightClickMenu() {
	} // TODO: Display and options? (extension on the display function with buttons)
	// add castle, add text, change color, add character

	public void editText() {
	} // TODO

	public void hoverDisplay() {
	} // TODO

	public void encrypt() {
	} // TODO

	public void decrypt() {
	} // TODO

	public void changeTexture() {
	} // TODO

}
