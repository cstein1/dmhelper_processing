package Resources;

import processing.core.PApplet;
import processing.core.PVector;
import java.awt.Color;

// Tool button for Design Screen

// Will we need a String->Tool hashmap?
public class ToolButton extends PApplet {
  PVector loc, size;
  Color ocol, ncol;
  String txt;
  
  public ToolButton(PVector _loc, PVector _size, 
         Color _ocolor, Color _ncolor,
         String _txt){ // original color vs new color
    loc = _loc;
    size = _size;
    ocol = _ocolor;
    ncol = _ncolor;
    txt = _txt;
  }
  
  public ToolButton(float x, float y, float sx, float sy,
         Color _ocolor, Color _ncolor,
         String _txt){ // original color vs new color
    loc = new PVector(floor(x),floor(y));
    size = new PVector(floor(sx),floor(sy));
    ocol = _ocolor;
    ncol = _ncolor;
    txt = _txt;
  }
  
  public void display(){
    if (hovering()){
      fill(ncol.getRGB());
    } else {
      fill(ocol.getRGB());
    }
    
    rectMode(CENTER);
    rect(loc.x, loc.y, size.x, size.y);
    fill(0,0,255);
    textSize(16);
    textAlign(CENTER);
    text(txt, loc.x, loc.y);
    
  }
  
  boolean hovering() {
    float hx = size.x/2;
    float hy = size.y/2;
    return (mouseX > loc.x - hx && mouseX < loc.x + hx) &&
           (mouseY > loc.y - hy && mouseY < loc.y + hy);
  }
}
